package com.yianke.pet.ui.activity;

import android.view.View;
import android.widget.TextView;

import com.yianke.pet.R;
import com.yianke.pet.base.BaseActivity;

import butterknife.Bind;
import butterknife.OnClick;

public class MissActivity extends BaseActivity
{

    @Bind(R.id.top_text)
    TextView mTopText;

    @Override
    protected int getResourceId()
    {
        return R.layout.activity_miss;
    }


    @Override
    public void initView()
    {
        super.initView();
        mTopText.setText("忘记密码");
    }


    @OnClick({R.id.top_back, R.id.miss_btn})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.top_back:
                finish();
                break;
            case R.id.miss_btn:
                finish();
                break;
            default:
                break;
        }
    }
}
